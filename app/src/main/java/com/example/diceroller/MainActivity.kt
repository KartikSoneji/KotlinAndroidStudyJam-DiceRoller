/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.diceroller

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


/**
 * This activity allows the user to roll a dice and view the result
 * on the screen.
 */
class MainActivity : AppCompatActivity() {
    companion object {
        private const val SHAKE_THRESHOLD = 1.15f
        private const val SHAKE_DELAY_MS = 500
    }

    private var sensorManager: SensorManager? = null
    private var accelerometer: Sensor? = null
    private var shakeDetector: SensorEventListener? = null

    private var lastShakeTimestamp: Long = 0

    /**
     * This method is called when the Activity is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // roll the dice when the button is clicked
        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener { rollDice() }

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        // if accelerometer is null then the device doesn't have one so ignore shake
        if (accelerometer != null)
            shakeDetector = object : SensorEventListener {
                override fun onSensorChanged(event: SensorEvent?) = handleShakeEvent(event!!)
                override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
            }
    }

    private fun handleShakeEvent(event: SensorEvent) {
        // get the x, y and z values for acceleration
        val gX = event.values[0] / SensorManager.GRAVITY_EARTH
        val gY = event.values[1] / SensorManager.GRAVITY_EARTH
        val gZ = event.values[2] / SensorManager.GRAVITY_EARTH

        // gForce will be close to 1 when there is no movement
        // use gForceSquared instead of gForce to prevent an expensive sqrt operation
        val gForceSquared = gX*gX + gY*gY + gZ*gZ

        if (gForceSquared > (SHAKE_THRESHOLD * SHAKE_THRESHOLD)) {
            val now = System.currentTimeMillis()
            // ignore shake events too close to each other
            if ((now - lastShakeTimestamp) >= SHAKE_DELAY_MS) {
                lastShakeTimestamp = now

                //perform action
                rollDice()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (accelerometer != null)
            sensorManager!!.registerListener(
                shakeDetector,
                accelerometer,
                SensorManager.SENSOR_DELAY_UI
            )
    }

    override fun onPause() {
        if (shakeDetector != null)
            sensorManager!!.unregisterListener(shakeDetector)

        super.onPause()
    }

    /**
     * Roll the dice and update the screen with the result.
     */
    private fun rollDice() {
        // Create new Dice object with 6 sides and roll it
        val dice = Dice(6)
        val diceRoll = dice.roll()

        // Update the screen with the dice roll
        val resultTextView: TextView = findViewById(R.id.textView)
        resultTextView.text = diceRoll.toString()
    }
}

/**
 * Dice with a fixed number of sides.
 */
class Dice(private val numSides: Int) {

    /**
     * Do a random dice roll and return the result.
     */
    fun roll(): Int {
        return (1..numSides).random()
    }
}